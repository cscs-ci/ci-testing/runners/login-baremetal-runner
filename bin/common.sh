# $1 function that should be retried (will be called through eval)
# $2 max retries (int)
# $3 sleep in seconds between retries - use 3 arguments for random sleeping: random min_sleep max_sleep
# $3 can be 3 arguments via RAND min_sleep max_sleep, but we continue counting with $4 for simplicity
# $4 function that should be called upon every failed try (will be called through eval)
function retry {
    local fct="$1"
    shift

    local max_retries="$1"
    shift

    local min_sleep="$1"
    local sleep_fct="sleep $1"
    shift
    if [[ "$min_sleep" == "random" ]] ; then
        local min_sleep="$1"
        shift
        local max_sleep="$1"
        shift
        local max_min_sleep_diff=$(( max_sleep - min_sleep ))
        sleep_fct="sleep \$(( RANDOM % max_min_sleep_diff + min ))"
    fi

    local failure_fct=""
    [[ -n "$1" ]] && failure_fct="$1" && shift

    local RETRY_COUNT=0
    while ! eval "$fct" ; do
        let ++RETRY_COUNT
        if [[ $RETRY_COUNT -ge max_retries ]] ; then
            exit $SYSTEM_FAILURE_EXIT_CODE
        fi
        eval "$failure_fct"
        eval "$sleep_fct"
    done
    return 0
}

# some systems set LD_PRELOAD, which is a variable that shouldn't propagate into the container...
unset LD_PRELOAD

b="\033[0;36m"
g="\033[0;32m"
r="\033[0;31m"
e="\033[0;90m"
x="\033[0m"

