#!/usr/bin/env bash

b="\033[0;36m"
g="\033[0;32m"
r="\033[0;31m"
e="\033[0;90m"
x="\033[0m"

CLEANENV=("env" \
    "-i" \
    "PATH=$PATH" \
    "SCRATCH=$SCRATCH" \
    "USER=$USER" \
    "HOME=$HOME"
)
[[ -v LD_LIBRARY_PATH ]] && CLEANENV+=("LD_LIBRARY_PATH=$LD_LIBRARY_PATH")
function export_and_collect {
    export $1
    CLEANENV+=("$1")
}

if [[ -v CUSTOM_ENV_CRAY_CUDA_MPS ]]; then
    export_and_collect CRAY_CUDA_MPS="$CUSTOM_ENV_CRAY_CUDA_MPS"
fi

if [[ -v CUSTOM_ENV_CUDA_VISIBLE_DEVICES ]]; then
    export_and_collect CUDA_VISIBLE_DEVICES="$CUSTOM_ENV_CUDA_VISIBLE_DEVICES"
fi

if [[ -v CUSTOM_ENV_PMI_FANOUT ]]; then
    export_and_collect PMI_FANOUT="$CUSTOM_ENV_PMI_FANOUT"
fi

if [[ -v CUSTOM_ENV_PMI_FANOUT_OFF_HOST ]]; then
    export_and_collect PMI_FANOUT_OFF_HOST="$CUSTOM_ENV_PMI_FANOUT_OFF_HOST"
fi

if [[ -v CUSTOM_ENV_PMI_TIME ]]; then
    export_and_collect PMI_TIME="$CUSTOM_ENV_PMI_TIME"
fi

if [[ -v CUSTOM_ENV_SALLOC_ACCOUNT ]]; then
    export_and_collect SALLOC_ACCOUNT="$CUSTOM_ENV_SALLOC_ACCOUNT"
fi

if [[ -v CUSTOM_ENV_SALLOC_ACCTG_FREQ ]]; then
    export_and_collect SALLOC_ACCTG_FREQ="$CUSTOM_ENV_SALLOC_ACCTG_FREQ"
fi

if [[ -v CUSTOM_ENV_SALLOC_BELL ]]; then
    export_and_collect SALLOC_BELL="$CUSTOM_ENV_SALLOC_BELL"
fi

if [[ -v CUSTOM_ENV_SALLOC_BURST_BUFFER ]]; then
    export_and_collect SALLOC_BURST_BUFFER="$CUSTOM_ENV_SALLOC_BURST_BUFFER"
fi

if [[ -v CUSTOM_ENV_SALLOC_CLUSTERS ]]; then
    export_and_collect SALLOC_CLUSTERS="$CUSTOM_ENV_SALLOC_CLUSTERS"
fi

if [[ -v CUSTOM_ENV_SALLOC_CONSTRAINT ]]; then
    export_and_collect SALLOC_CONSTRAINT="$CUSTOM_ENV_SALLOC_CONSTRAINT"
fi

if [[ -v CUSTOM_ENV_SALLOC_CORE_SPEC ]]; then
    export_and_collect SALLOC_CORE_SPEC="$CUSTOM_ENV_SALLOC_CORE_SPEC"
fi

if [[ -v CUSTOM_ENV_SALLOC_CPUS_PER_GPU ]]; then
    export_and_collect SALLOC_CPUS_PER_GPU="$CUSTOM_ENV_SALLOC_CPUS_PER_GPU"
fi

if [[ -v CUSTOM_ENV_SALLOC_DEBUG ]]; then
    export_and_collect SALLOC_DEBUG="$CUSTOM_ENV_SALLOC_DEBUG"
fi

if [[ -v CUSTOM_ENV_SALLOC_DELAY_BOOT ]]; then
    export_and_collect SALLOC_DELAY_BOOT="$CUSTOM_ENV_SALLOC_DELAY_BOOT"
fi

if [[ -v CUSTOM_ENV_SALLOC_EXCLUSIVE ]]; then
    export_and_collect SALLOC_EXCLUSIVE="$CUSTOM_ENV_SALLOC_EXCLUSIVE"
fi

if [[ -v CUSTOM_ENV_SALLOC_GPU_BIND ]]; then
    export_and_collect SALLOC_GPU_BIND="$CUSTOM_ENV_SALLOC_GPU_BIND"
fi

if [[ -v CUSTOM_ENV_SALLOC_GPU_FREQ ]]; then
    export_and_collect SALLOC_GPU_FREQ="$CUSTOM_ENV_SALLOC_GPU_FREQ"
fi

if [[ -v CUSTOM_ENV_SALLOC_GPUS ]]; then
    export_and_collect SALLOC_GPUS="$CUSTOM_ENV_SALLOC_GPUS"
fi

if [[ -v CUSTOM_ENV_SALLOC_GPUS_PER_NODE ]]; then
    export_and_collect SALLOC_GPUS_PER_NODE="$CUSTOM_ENV_SALLOC_GPUS_PER_NODE"
fi

if [[ -v CUSTOM_ENV_SALLOC_GPUS_PER_TASK ]]; then
    export_and_collect SALLOC_GPUS_PER_TASK="$CUSTOM_ENV_SALLOC_GPUS_PER_TASK"
fi

if [[ -v CUSTOM_ENV_SALLOC_GRES ]]; then
    export_and_collect SALLOC_GRES="$CUSTOM_ENV_SALLOC_GRES"
fi

if [[ -v CUSTOM_ENV_SALLOC_GRES_FLAGS ]]; then
    export_and_collect SALLOC_GRES_FLAGS="$CUSTOM_ENV_SALLOC_GRES_FLAGS"
fi

if [[ -v CUSTOM_ENV_SALLOC_HINT ]]; then
    export_and_collect SALLOC_HINT="$CUSTOM_ENV_SALLOC_HINT"
fi

if [[ -v CUSTOM_ENV_SALLOC_IMMEDIATE ]]; then
    export_and_collect SALLOC_IMMEDIATE="$CUSTOM_ENV_SALLOC_IMMEDIATE"
fi

if [[ -v CUSTOM_ENV_SALLOC_KILL_CMD ]]; then
    export_and_collect SALLOC_KILL_CMD="$CUSTOM_ENV_SALLOC_KILL_CMD"
fi

if [[ -v CUSTOM_ENV_SALLOC_MEM_BIND ]]; then
    export_and_collect SALLOC_MEM_BIND="$CUSTOM_ENV_SALLOC_MEM_BIND"
fi

if [[ -v CUSTOM_ENV_SALLOC_MEM_PER_CPU ]]; then
    export_and_collect SALLOC_MEM_PER_CPU="$CUSTOM_ENV_SALLOC_MEM_PER_CPU"
fi

if [[ -v CUSTOM_ENV_SALLOC_MEM_PER_GPU ]]; then
    export_and_collect SALLOC_MEM_PER_GPU="$CUSTOM_ENV_SALLOC_MEM_PER_GPU"
fi

if [[ -v CUSTOM_ENV_SALLOC_MEM_PER_NODE ]]; then
    export_and_collect SALLOC_MEM_PER_NODE="$CUSTOM_ENV_SALLOC_MEM_PER_NODE"
fi

if [[ -v CUSTOM_ENV_SALLOC_NETWORK ]]; then
    export_and_collect SALLOC_NETWORK="$CUSTOM_ENV_SALLOC_NETWORK"
fi

if [[ -v CUSTOM_ENV_SALLOC_NO_BELL ]]; then
    export_and_collect SALLOC_NO_BELL="$CUSTOM_ENV_SALLOC_NO_BELL"
fi

if [[ -v CUSTOM_ENV_SALLOC_NO_KILL ]]; then
    export_and_collect SALLOC_NO_KILL="$CUSTOM_ENV_SALLOC_NO_KILL"
fi

if [[ -v CUSTOM_ENV_SALLOC_OVERCOMMIT ]]; then
    export_and_collect SALLOC_OVERCOMMIT="$CUSTOM_ENV_SALLOC_OVERCOMMIT"
fi

if [[ -v CUSTOM_ENV_SALLOC_PARTITION ]]; then
    export_and_collect SALLOC_PARTITION="$CUSTOM_ENV_SALLOC_PARTITION"
fi

if [[ -v CUSTOM_ENV_SALLOC_POWER ]]; then
    export_and_collect SALLOC_POWER="$CUSTOM_ENV_SALLOC_POWER"
fi

if [[ -v CUSTOM_ENV_SALLOC_PROFILE ]]; then
    export_and_collect SALLOC_PROFILE="$CUSTOM_ENV_SALLOC_PROFILE"
fi

if [[ -v CUSTOM_ENV_SALLOC_QOS ]]; then
    export_and_collect SALLOC_QOS="$CUSTOM_ENV_SALLOC_QOS"
fi

if [[ -v CUSTOM_ENV_SALLOC_REQ_SWITCH ]]; then
    export_and_collect SALLOC_REQ_SWITCH="$CUSTOM_ENV_SALLOC_REQ_SWITCH"
fi

if [[ -v CUSTOM_ENV_SALLOC_RESERVATION ]]; then
    export_and_collect SALLOC_RESERVATION="$CUSTOM_ENV_SALLOC_RESERVATION"
fi

if [[ -v CUSTOM_ENV_SALLOC_SIGNAL ]]; then
    export_and_collect SALLOC_SIGNAL="$CUSTOM_ENV_SALLOC_SIGNAL"
fi

if [[ -v CUSTOM_ENV_SALLOC_SPREAD_JOB ]]; then
    export_and_collect SALLOC_SPREAD_JOB="$CUSTOM_ENV_SALLOC_SPREAD_JOB"
fi

if [[ -v CUSTOM_ENV_SALLOC_THREAD_SPEC ]]; then
    export_and_collect SALLOC_THREAD_SPEC="$CUSTOM_ENV_SALLOC_THREAD_SPEC"
fi

if [[ -v CUSTOM_ENV_SALLOC_TIMELIMIT ]]; then
    export_and_collect SALLOC_TIMELIMIT="$CUSTOM_ENV_SALLOC_TIMELIMIT"
fi

if [[ -v CUSTOM_ENV_SALLOC_USE_MIN_NODES ]]; then
    export_and_collect SALLOC_USE_MIN_NODES="$CUSTOM_ENV_SALLOC_USE_MIN_NODES"
fi

if [[ -v CUSTOM_ENV_SALLOC_WAIT_ALL_NODES ]]; then
    export_and_collect SALLOC_WAIT_ALL_NODES="$CUSTOM_ENV_SALLOC_WAIT_ALL_NODES"
fi

if [[ -v CUSTOM_ENV_SALLOC_WAIT4SWITCH ]]; then
    export_and_collect SALLOC_WAIT4SWITCH="$CUSTOM_ENV_SALLOC_WAIT4SWITCH"
fi

if [[ -v CUSTOM_ENV_SALLOC_WCKEY ]]; then
    export_and_collect SALLOC_WCKEY="$CUSTOM_ENV_SALLOC_WCKEY"
fi

if [[ -v CUSTOM_ENV_SLURM_ACCOUNT ]]; then
    export_and_collect SLURM_ACCOUNT="$CUSTOM_ENV_SLURM_ACCOUNT"
fi

if [[ -v CUSTOM_ENV_SLURM_ACCTG_FREQ ]]; then
    export_and_collect SLURM_ACCTG_FREQ="$CUSTOM_ENV_SLURM_ACCTG_FREQ"
fi

if [[ -v CUSTOM_ENV_SLURM_BCAST ]]; then
    export_and_collect SLURM_BCAST="$CUSTOM_ENV_SLURM_BCAST"
fi

if [[ -v CUSTOM_ENV_SLURM_BURST_BUFFER ]]; then
    export_and_collect SLURM_BURST_BUFFER="$CUSTOM_ENV_SLURM_BURST_BUFFER"
fi

if [[ -v CUSTOM_ENV_SLURM_CLUSTERS ]]; then
    export_and_collect SLURM_CLUSTERS="$CUSTOM_ENV_SLURM_CLUSTERS"
fi

if [[ -v CUSTOM_ENV_SLURM_COMPRESS ]]; then
    export_and_collect SLURM_COMPRESS="$CUSTOM_ENV_SLURM_COMPRESS"
fi

if [[ -v CUSTOM_ENV_SLURM_CONF ]]; then
    export_and_collect SLURM_CONF="$CUSTOM_ENV_SLURM_CONF"
fi

if [[ -v CUSTOM_ENV_SLURM_CONSTRAINT ]]; then
    export_and_collect SLURM_CONSTRAINT="$CUSTOM_ENV_SLURM_CONSTRAINT"
fi

if [[ -v CUSTOM_ENV_SLURM_CORE_SPEC ]]; then
    export_and_collect SLURM_CORE_SPEC="$CUSTOM_ENV_SLURM_CORE_SPEC"
fi

if [[ -v CUSTOM_ENV_SLURM_CPU_BIND ]]; then
    export_and_collect SLURM_CPU_BIND="$CUSTOM_ENV_SLURM_CPU_BIND"
fi

if [[ -v CUSTOM_ENV_SLURM_CPU_FREQ_REQ ]]; then
    export_and_collect SLURM_CPU_FREQ_REQ="$CUSTOM_ENV_SLURM_CPU_FREQ_REQ"
fi

if [[ -v CUSTOM_ENV_SLURM_CPUS_PER_GPU ]]; then
    export_and_collect SLURM_CPUS_PER_GPU="$CUSTOM_ENV_SLURM_CPUS_PER_GPU"
fi

if [[ -v CUSTOM_ENV_SLURM_CPUS_PER_TASK ]]; then
    export_and_collect SLURM_CPUS_PER_TASK="$CUSTOM_ENV_SLURM_CPUS_PER_TASK"
fi

if [[ -v CUSTOM_ENV_SLURM_DEBUG ]]; then
    export_and_collect SLURM_DEBUG="$CUSTOM_ENV_SLURM_DEBUG"
fi

if [[ -v CUSTOM_ENV_SLURM_DELAY_BOOT ]]; then
    export_and_collect SLURM_DELAY_BOOT="$CUSTOM_ENV_SLURM_DELAY_BOOT"
fi

if [[ -v CUSTOM_ENV_SLURM_DEPENDENCY ]]; then
    export_and_collect SLURM_DEPENDENCY="$CUSTOM_ENV_SLURM_DEPENDENCY"
fi

if [[ -v CUSTOM_ENV_SLURM_DISABLE_STATUS ]]; then
    export_and_collect SLURM_DISABLE_STATUS="$CUSTOM_ENV_SLURM_DISABLE_STATUS"
fi

if [[ -v CUSTOM_ENV_SLURM_DIST_PLANESIZE ]]; then
    export_and_collect SLURM_DIST_PLANESIZE="$CUSTOM_ENV_SLURM_DIST_PLANESIZE"
fi

if [[ -v CUSTOM_ENV_SLURM_DISTRIBUTION ]]; then
    export_and_collect SLURM_DISTRIBUTION="$CUSTOM_ENV_SLURM_DISTRIBUTION"
fi

if [[ -v CUSTOM_ENV_SLURM_EPILOG ]]; then
    export_and_collect SLURM_EPILOG="$CUSTOM_ENV_SLURM_EPILOG"
fi

if [[ -v CUSTOM_ENV_SLURM_EXACT ]]; then
    export_and_collect SLURM_EXACT="$CUSTOM_ENV_SLURM_EXACT"
fi

if [[ -v CUSTOM_ENV_SLURM_EXCLUSIVE ]]; then
    export_and_collect SLURM_EXCLUSIVE="$CUSTOM_ENV_SLURM_EXCLUSIVE"
fi

if [[ -v CUSTOM_ENV_SLURM_EXIT_ERROR ]]; then
    export_and_collect SLURM_EXIT_ERROR="$CUSTOM_ENV_SLURM_EXIT_ERROR"
fi

if [[ -v CUSTOM_ENV_SLURM_EXIT_IMMEDIATE ]]; then
    export_and_collect SLURM_EXIT_IMMEDIATE="$CUSTOM_ENV_SLURM_EXIT_IMMEDIATE"
fi

if [[ -v CUSTOM_ENV_SLURM_EXPORT_ENV ]]; then
    export_and_collect SLURM_EXPORT_ENV="$CUSTOM_ENV_SLURM_EXPORT_ENV"
fi

if [[ -v CUSTOM_ENV_SLURM_GPU_BIND ]]; then
    export_and_collect SLURM_GPU_BIND="$CUSTOM_ENV_SLURM_GPU_BIND"
fi

if [[ -v CUSTOM_ENV_SLURM_GPU_FREQ ]]; then
    export_and_collect SLURM_GPU_FREQ="$CUSTOM_ENV_SLURM_GPU_FREQ"
fi

if [[ -v CUSTOM_ENV_SLURM_GPUS ]]; then
    export_and_collect SLURM_GPUS="$CUSTOM_ENV_SLURM_GPUS"
fi

if [[ -v CUSTOM_ENV_SLURM_GPUS_PER_NODE ]]; then
    export_and_collect SLURM_GPUS_PER_NODE="$CUSTOM_ENV_SLURM_GPUS_PER_NODE"
fi

if [[ -v CUSTOM_ENV_SLURM_GPUS_PER_TASK ]]; then
    export_and_collect SLURM_GPUS_PER_TASK="$CUSTOM_ENV_SLURM_GPUS_PER_TASK"
fi

if [[ -v CUSTOM_ENV_SLURM_GRES ]]; then
    export_and_collect SLURM_GRES="$CUSTOM_ENV_SLURM_GRES"
fi

if [[ -v CUSTOM_ENV_SLURM_GRES_FLAGS ]]; then
    export_and_collect SLURM_GRES_FLAGS="$CUSTOM_ENV_SLURM_GRES_FLAGS"
fi

if [[ -v CUSTOM_ENV_SLURM_HINT ]]; then
    export_and_collect SLURM_HINT="$CUSTOM_ENV_SLURM_HINT"
fi

if [[ -v CUSTOM_ENV_SLURM_IMMEDIATE ]]; then
    export_and_collect SLURM_IMMEDIATE="$CUSTOM_ENV_SLURM_IMMEDIATE"
fi

if [[ -v CUSTOM_ENV_SLURM_JOB_ID ]]; then
    export_and_collect SLURM_JOB_ID="$CUSTOM_ENV_SLURM_JOB_ID"
fi

if [[ -v CUSTOM_ENV_SLURM_JOB_NAME ]]; then
    export_and_collect SLURM_JOB_NAME="$CUSTOM_ENV_SLURM_JOB_NAME"
fi

if [[ -v CUSTOM_ENV_SLURM_JOB_NODELIST ]]; then
    export_and_collect SLURM_JOB_NODELIST="$CUSTOM_ENV_SLURM_JOB_NODELIST"
fi

if [[ -v CUSTOM_ENV_SLURM_JOB_NUM_NODES ]]; then
    export_and_collect SLURM_JOB_NUM_NODES="$CUSTOM_ENV_SLURM_JOB_NUM_NODES"
fi

if [[ -v CUSTOM_ENV_SLURM_KILL_BAD_EXIT ]]; then
    export_and_collect SLURM_KILL_BAD_EXIT="$CUSTOM_ENV_SLURM_KILL_BAD_EXIT"
fi

if [[ -v CUSTOM_ENV_SLURM_LABELIO ]]; then
    export_and_collect SLURM_LABELIO="$CUSTOM_ENV_SLURM_LABELIO"
fi

if [[ -v CUSTOM_ENV_SLURM_MEM_BIND ]]; then
    export_and_collect SLURM_MEM_BIND="$CUSTOM_ENV_SLURM_MEM_BIND"
fi

if [[ -v CUSTOM_ENV_SLURM_MEM_PER_CPU ]]; then
    export_and_collect SLURM_MEM_PER_CPU="$CUSTOM_ENV_SLURM_MEM_PER_CPU"
fi

if [[ -v CUSTOM_ENV_SLURM_MEM_PER_GPU ]]; then
    export_and_collect SLURM_MEM_PER_GPU="$CUSTOM_ENV_SLURM_MEM_PER_GPU"
fi

if [[ -v CUSTOM_ENV_SLURM_MEM_PER_NODE ]]; then
    export_and_collect SLURM_MEM_PER_NODE="$CUSTOM_ENV_SLURM_MEM_PER_NODE"
fi

if [[ -v CUSTOM_ENV_SLURM_MPI_TYPE ]]; then
    export_and_collect SLURM_MPI_TYPE="$CUSTOM_ENV_SLURM_MPI_TYPE"
fi

if [[ -v CUSTOM_ENV_SLURM_NETWORK ]]; then
    export_and_collect SLURM_NETWORK="$CUSTOM_ENV_SLURM_NETWORK"
fi

if [[ -v CUSTOM_ENV_SLURM_NNODES ]]; then
    export_and_collect SLURM_NNODES="$CUSTOM_ENV_SLURM_NNODES"
fi

if [[ -v CUSTOM_ENV_SLURM_NO_KILL ]]; then
    export_and_collect SLURM_NO_KILL="$CUSTOM_ENV_SLURM_NO_KILL"
fi

if [[ -v CUSTOM_ENV_SLURM_NPROCS ]]; then
    export_and_collect SLURM_NPROCS="$CUSTOM_ENV_SLURM_NPROCS"
fi

if [[ -v CUSTOM_ENV_SLURM_NTASKS ]]; then
    export_and_collect SLURM_NTASKS="$CUSTOM_ENV_SLURM_NTASKS"
fi

if [[ -v CUSTOM_ENV_SLURM_NTASKS_PER_CORE ]]; then
    export_and_collect SLURM_NTASKS_PER_CORE="$CUSTOM_ENV_SLURM_NTASKS_PER_CORE"
fi

if [[ -v CUSTOM_ENV_SLURM_NTASKS_PER_GPU ]]; then
    export_and_collect SLURM_NTASKS_PER_GPU="$CUSTOM_ENV_SLURM_NTASKS_PER_GPU"
fi

if [[ -v CUSTOM_ENV_SLURM_NTASKS_PER_NODE ]]; then
    export_and_collect SLURM_NTASKS_PER_NODE="$CUSTOM_ENV_SLURM_NTASKS_PER_NODE"
fi

if [[ -v CUSTOM_ENV_SLURM_NTASKS_PER_SOCKET ]]; then
    export_and_collect SLURM_NTASKS_PER_SOCKET="$CUSTOM_ENV_SLURM_NTASKS_PER_SOCKET"
fi

if [[ -v CUSTOM_ENV_SLURM_OPEN_MODE ]]; then
    export_and_collect SLURM_OPEN_MODE="$CUSTOM_ENV_SLURM_OPEN_MODE"
fi

if [[ -v CUSTOM_ENV_SLURM_OVERCOMMIT ]]; then
    export_and_collect SLURM_OVERCOMMIT="$CUSTOM_ENV_SLURM_OVERCOMMIT"
fi

if [[ -v CUSTOM_ENV_SLURM_OVERLAP ]]; then
    export_and_collect SLURM_OVERLAP="$CUSTOM_ENV_SLURM_OVERLAP"
fi

if [[ -v CUSTOM_ENV_SLURM_PARTITION ]]; then
    export_and_collect SLURM_PARTITION="$CUSTOM_ENV_SLURM_PARTITION"
fi

if [[ -v CUSTOM_ENV_SLURM_PMI_KVS_NO_DUP_KEYS ]]; then
    export_and_collect SLURM_PMI_KVS_NO_DUP_KEYS="$CUSTOM_ENV_SLURM_PMI_KVS_NO_DUP_KEYS"
fi

if [[ -v CUSTOM_ENV_SLURM_POWER ]]; then
    export_and_collect SLURM_POWER="$CUSTOM_ENV_SLURM_POWER"
fi

if [[ -v CUSTOM_ENV_SLURM_PROFILE ]]; then
    export_and_collect SLURM_PROFILE="$CUSTOM_ENV_SLURM_PROFILE"
fi

if [[ -v CUSTOM_ENV_SLURM_PROLOG ]]; then
    export_and_collect SLURM_PROLOG="$CUSTOM_ENV_SLURM_PROLOG"
fi

if [[ -v CUSTOM_ENV_SLURM_QOS ]]; then
    export_and_collect SLURM_QOS="$CUSTOM_ENV_SLURM_QOS"
fi

if [[ -v CUSTOM_ENV_SLURM_REMOTE_CWD ]]; then
    export_and_collect SLURM_REMOTE_CWD="$CUSTOM_ENV_SLURM_REMOTE_CWD"
fi

if [[ -v CUSTOM_ENV_SLURM_REQ_SWITCH ]]; then
    export_and_collect SLURM_REQ_SWITCH="$CUSTOM_ENV_SLURM_REQ_SWITCH"
fi

if [[ -v CUSTOM_ENV_SLURM_RESERVATION ]]; then
    export_and_collect SLURM_RESERVATION="$CUSTOM_ENV_SLURM_RESERVATION"
fi

if [[ -v CUSTOM_ENV_SLURM_RESV_PORTS ]]; then
    export_and_collect SLURM_RESV_PORTS="$CUSTOM_ENV_SLURM_RESV_PORTS"
fi

if [[ -v CUSTOM_ENV_SLURM_SIGNAL ]]; then
    export_and_collect SLURM_SIGNAL="$CUSTOM_ENV_SLURM_SIGNAL"
fi

if [[ -v CUSTOM_ENV_SLURM_SPREAD_JOB ]]; then
    export_and_collect SLURM_SPREAD_JOB="$CUSTOM_ENV_SLURM_SPREAD_JOB"
fi

if [[ -v CUSTOM_ENV_SLURM_SRUN_REDUCE_TASK_EXIT_MSG ]]; then
    export_and_collect SLURM_SRUN_REDUCE_TASK_EXIT_MSG="$CUSTOM_ENV_SLURM_SRUN_REDUCE_TASK_EXIT_MSG"
fi

if [[ -v CUSTOM_ENV_SLURM_STDERRMODE ]]; then
    export_and_collect SLURM_STDERRMODE="$CUSTOM_ENV_SLURM_STDERRMODE"
fi

if [[ -v CUSTOM_ENV_SLURM_STDINMODE ]]; then
    export_and_collect SLURM_STDINMODE="$CUSTOM_ENV_SLURM_STDINMODE"
fi

if [[ -v CUSTOM_ENV_SLURM_STDOUTMODE ]]; then
    export_and_collect SLURM_STDOUTMODE="$CUSTOM_ENV_SLURM_STDOUTMODE"
fi

if [[ -v CUSTOM_ENV_SLURM_STEP_GRES ]]; then
    export_and_collect SLURM_STEP_GRES="$CUSTOM_ENV_SLURM_STEP_GRES"
fi

if [[ -v CUSTOM_ENV_SLURM_STEP_KILLED_MSG_NODE_ID ]]; then
    export_and_collect SLURM_STEP_KILLED_MSG_NODE_ID="$CUSTOM_ENV_SLURM_STEP_KILLED_MSG_NODE_ID"
fi

if [[ -v CUSTOM_ENV_SLURM_TASK_EPILOG ]]; then
    export_and_collect SLURM_TASK_EPILOG="$CUSTOM_ENV_SLURM_TASK_EPILOG"
fi

if [[ -v CUSTOM_ENV_SLURM_TASK_PROLOG ]]; then
    export_and_collect SLURM_TASK_PROLOG="$CUSTOM_ENV_SLURM_TASK_PROLOG"
fi

if [[ -v CUSTOM_ENV_SLURM_TEST_EXEC ]]; then
    export_and_collect SLURM_TEST_EXEC="$CUSTOM_ENV_SLURM_TEST_EXEC"
fi

if [[ -v CUSTOM_ENV_SLURM_THREAD_SPEC ]]; then
    export_and_collect SLURM_THREAD_SPEC="$CUSTOM_ENV_SLURM_THREAD_SPEC"
fi

if [[ -v CUSTOM_ENV_SLURM_THREADS ]]; then
    export_and_collect SLURM_THREADS="$CUSTOM_ENV_SLURM_THREADS"
fi

if [[ -v CUSTOM_ENV_SLURM_THREADS_PER_CORE ]]; then
    export_and_collect SLURM_THREADS_PER_CORE="$CUSTOM_ENV_SLURM_THREADS_PER_CORE"
fi

if [[ -v CUSTOM_ENV_SLURM_TIMELIMIT ]]; then
    export_and_collect SLURM_TIMELIMIT="$CUSTOM_ENV_SLURM_TIMELIMIT"
fi

if [[ -v CUSTOM_ENV_SLURM_UNBUFFEREDIO ]]; then
    export_and_collect SLURM_UNBUFFEREDIO="$CUSTOM_ENV_SLURM_UNBUFFEREDIO"
fi

if [[ -v CUSTOM_ENV_SLURM_USE_MIN_NODES ]]; then
    export_and_collect SLURM_USE_MIN_NODES="$CUSTOM_ENV_SLURM_USE_MIN_NODES"
fi

if [[ -v CUSTOM_ENV_SLURM_WAIT ]]; then
    export_and_collect SLURM_WAIT="$CUSTOM_ENV_SLURM_WAIT"
fi

if [[ -v CUSTOM_ENV_SLURM_WAIT4SWITCH ]]; then
    export_and_collect SLURM_WAIT4SWITCH="$CUSTOM_ENV_SLURM_WAIT4SWITCH"
fi

if [[ -v CUSTOM_ENV_SLURM_WCKEY ]]; then
    export_and_collect SLURM_WCKEY="$CUSTOM_ENV_SLURM_WCKEY"
fi

if [[ -v CUSTOM_ENV_SLURM_WHOLE ]]; then
    export_and_collect SLURM_WHOLE="$CUSTOM_ENV_SLURM_WHOLE"
fi

if [[ -v CUSTOM_ENV_SLURM_WORKING_DIR ]]; then
    export_and_collect SLURM_WORKING_DIR="$CUSTOM_ENV_SLURM_WORKING_DIR"
fi

if [[ -v CUSTOM_ENV_SLURMD_DEBUG ]]; then
    export_and_collect SLURMD_DEBUG="$CUSTOM_ENV_SLURMD_DEBUG"
fi

if [[ -v CUSTOM_ENV_SRUN_EXPORT_ENV ]]; then
    export_and_collect SRUN_EXPORT_ENV="$CUSTOM_ENV_SRUN_EXPORT_ENV"
fi

# Verbose output?
if [[ $1 == "YES" ]]; then
    if [[ -v CRAY_CUDA_MPS ]]; then
        echo -e "export ${b}CRAY_CUDA_MPS${x}=${b}$CRAY_CUDA_MPS${x}"
    fi

    if [[ -v CUDA_VISIBLE_DEVICES ]]; then
        echo -e "export ${b}CUDA_VISIBLE_DEVICES${x}=${b}$CUDA_VISIBLE_DEVICES${x}"
    fi

    if [[ -v PMI_FANOUT ]]; then
        echo -e "export ${b}PMI_FANOUT${x}=${b}$PMI_FANOUT${x}"
    fi

    if [[ -v PMI_FANOUT_OFF_HOST ]]; then
        echo -e "export ${b}PMI_FANOUT_OFF_HOST${x}=${b}$PMI_FANOUT_OFF_HOST${x}"
    fi

    if [[ -v PMI_TIME ]]; then
        echo -e "export ${b}PMI_TIME${x}=${b}$PMI_TIME${x}"
    fi

    if [[ -v SALLOC_ACCOUNT ]]; then
        echo -e "export ${b}SALLOC_ACCOUNT${x}=${b}$SALLOC_ACCOUNT${x}"
    fi

    if [[ -v SALLOC_ACCTG_FREQ ]]; then
        echo -e "export ${b}SALLOC_ACCTG_FREQ${x}=${b}$SALLOC_ACCTG_FREQ${x}"
    fi

    if [[ -v SALLOC_BELL ]]; then
        echo -e "export ${b}SALLOC_BELL${x}=${b}$SALLOC_BELL${x}"
    fi

    if [[ -v SALLOC_BURST_BUFFER ]]; then
        echo -e "export ${b}SALLOC_BURST_BUFFER${x}=${b}$SALLOC_BURST_BUFFER${x}"
    fi

    if [[ -v SALLOC_CLUSTERS ]]; then
        echo -e "export ${b}SALLOC_CLUSTERS${x}=${b}$SALLOC_CLUSTERS${x}"
    fi

    if [[ -v SALLOC_CONSTRAINT ]]; then
        echo -e "export ${b}SALLOC_CONSTRAINT${x}=${b}$SALLOC_CONSTRAINT${x}"
    fi

    if [[ -v SALLOC_CORE_SPEC ]]; then
        echo -e "export ${b}SALLOC_CORE_SPEC${x}=${b}$SALLOC_CORE_SPEC${x}"
    fi

    if [[ -v SALLOC_CPUS_PER_GPU ]]; then
        echo -e "export ${b}SALLOC_CPUS_PER_GPU${x}=${b}$SALLOC_CPUS_PER_GPU${x}"
    fi

    if [[ -v SALLOC_DEBUG ]]; then
        echo -e "export ${b}SALLOC_DEBUG${x}=${b}$SALLOC_DEBUG${x}"
    fi

    if [[ -v SALLOC_DELAY_BOOT ]]; then
        echo -e "export ${b}SALLOC_DELAY_BOOT${x}=${b}$SALLOC_DELAY_BOOT${x}"
    fi

    if [[ -v SALLOC_EXCLUSIVE ]]; then
        echo -e "export ${b}SALLOC_EXCLUSIVE${x}=${b}$SALLOC_EXCLUSIVE${x}"
    fi

    if [[ -v SALLOC_GPU_BIND ]]; then
        echo -e "export ${b}SALLOC_GPU_BIND${x}=${b}$SALLOC_GPU_BIND${x}"
    fi

    if [[ -v SALLOC_GPU_FREQ ]]; then
        echo -e "export ${b}SALLOC_GPU_FREQ${x}=${b}$SALLOC_GPU_FREQ${x}"
    fi

    if [[ -v SALLOC_GPUS ]]; then
        echo -e "export ${b}SALLOC_GPUS${x}=${b}$SALLOC_GPUS${x}"
    fi

    if [[ -v SALLOC_GPUS_PER_NODE ]]; then
        echo -e "export ${b}SALLOC_GPUS_PER_NODE${x}=${b}$SALLOC_GPUS_PER_NODE${x}"
    fi

    if [[ -v SALLOC_GPUS_PER_TASK ]]; then
        echo -e "export ${b}SALLOC_GPUS_PER_TASK${x}=${b}$SALLOC_GPUS_PER_TASK${x}"
    fi

    if [[ -v SALLOC_GRES ]]; then
        echo -e "export ${b}SALLOC_GRES${x}=${b}$SALLOC_GRES${x}"
    fi

    if [[ -v SALLOC_GRES_FLAGS ]]; then
        echo -e "export ${b}SALLOC_GRES_FLAGS${x}=${b}$SALLOC_GRES_FLAGS${x}"
    fi

    if [[ -v SALLOC_HINT ]]; then
        echo -e "export ${b}SALLOC_HINT${x}=${b}$SALLOC_HINT${x}"
    fi

    if [[ -v SALLOC_IMMEDIATE ]]; then
        echo -e "export ${b}SALLOC_IMMEDIATE${x}=${b}$SALLOC_IMMEDIATE${x}"
    fi

    if [[ -v SALLOC_KILL_CMD ]]; then
        echo -e "export ${b}SALLOC_KILL_CMD${x}=${b}$SALLOC_KILL_CMD${x}"
    fi

    if [[ -v SALLOC_MEM_BIND ]]; then
        echo -e "export ${b}SALLOC_MEM_BIND${x}=${b}$SALLOC_MEM_BIND${x}"
    fi

    if [[ -v SALLOC_MEM_PER_CPU ]]; then
        echo -e "export ${b}SALLOC_MEM_PER_CPU${x}=${b}$SALLOC_MEM_PER_CPU${x}"
    fi

    if [[ -v SALLOC_MEM_PER_GPU ]]; then
        echo -e "export ${b}SALLOC_MEM_PER_GPU${x}=${b}$SALLOC_MEM_PER_GPU${x}"
    fi

    if [[ -v SALLOC_MEM_PER_NODE ]]; then
        echo -e "export ${b}SALLOC_MEM_PER_NODE${x}=${b}$SALLOC_MEM_PER_NODE${x}"
    fi

    if [[ -v SALLOC_NETWORK ]]; then
        echo -e "export ${b}SALLOC_NETWORK${x}=${b}$SALLOC_NETWORK${x}"
    fi

    if [[ -v SALLOC_NO_BELL ]]; then
        echo -e "export ${b}SALLOC_NO_BELL${x}=${b}$SALLOC_NO_BELL${x}"
    fi

    if [[ -v SALLOC_NO_KILL ]]; then
        echo -e "export ${b}SALLOC_NO_KILL${x}=${b}$SALLOC_NO_KILL${x}"
    fi

    if [[ -v SALLOC_OVERCOMMIT ]]; then
        echo -e "export ${b}SALLOC_OVERCOMMIT${x}=${b}$SALLOC_OVERCOMMIT${x}"
    fi

    if [[ -v SALLOC_PARTITION ]]; then
        echo -e "export ${b}SALLOC_PARTITION${x}=${b}$SALLOC_PARTITION${x}"
    fi

    if [[ -v SALLOC_POWER ]]; then
        echo -e "export ${b}SALLOC_POWER${x}=${b}$SALLOC_POWER${x}"
    fi

    if [[ -v SALLOC_PROFILE ]]; then
        echo -e "export ${b}SALLOC_PROFILE${x}=${b}$SALLOC_PROFILE${x}"
    fi

    if [[ -v SALLOC_QOS ]]; then
        echo -e "export ${b}SALLOC_QOS${x}=${b}$SALLOC_QOS${x}"
    fi

    if [[ -v SALLOC_REQ_SWITCH ]]; then
        echo -e "export ${b}SALLOC_REQ_SWITCH${x}=${b}$SALLOC_REQ_SWITCH${x}"
    fi

    if [[ -v SALLOC_RESERVATION ]]; then
        echo -e "export ${b}SALLOC_RESERVATION${x}=${b}$SALLOC_RESERVATION${x}"
    fi

    if [[ -v SALLOC_SIGNAL ]]; then
        echo -e "export ${b}SALLOC_SIGNAL${x}=${b}$SALLOC_SIGNAL${x}"
    fi

    if [[ -v SALLOC_SPREAD_JOB ]]; then
        echo -e "export ${b}SALLOC_SPREAD_JOB${x}=${b}$SALLOC_SPREAD_JOB${x}"
    fi

    if [[ -v SALLOC_THREAD_SPEC ]]; then
        echo -e "export ${b}SALLOC_THREAD_SPEC${x}=${b}$SALLOC_THREAD_SPEC${x}"
    fi

    if [[ -v SALLOC_TIMELIMIT ]]; then
        echo -e "export ${b}SALLOC_TIMELIMIT${x}=${b}$SALLOC_TIMELIMIT${x}"
    fi

    if [[ -v SALLOC_USE_MIN_NODES ]]; then
        echo -e "export ${b}SALLOC_USE_MIN_NODES${x}=${b}$SALLOC_USE_MIN_NODES${x}"
    fi

    if [[ -v SALLOC_WAIT_ALL_NODES ]]; then
        echo -e "export ${b}SALLOC_WAIT_ALL_NODES${x}=${b}$SALLOC_WAIT_ALL_NODES${x}"
    fi

    if [[ -v SALLOC_WAIT4SWITCH ]]; then
        echo -e "export ${b}SALLOC_WAIT4SWITCH${x}=${b}$SALLOC_WAIT4SWITCH${x}"
    fi

    if [[ -v SALLOC_WCKEY ]]; then
        echo -e "export ${b}SALLOC_WCKEY${x}=${b}$SALLOC_WCKEY${x}"
    fi

    if [[ -v SLURM_ACCOUNT ]]; then
        echo -e "export ${b}SLURM_ACCOUNT${x}=${b}$SLURM_ACCOUNT${x}"
    fi

    if [[ -v SLURM_ACCTG_FREQ ]]; then
        echo -e "export ${b}SLURM_ACCTG_FREQ${x}=${b}$SLURM_ACCTG_FREQ${x}"
    fi

    if [[ -v SLURM_BCAST ]]; then
        echo -e "export ${b}SLURM_BCAST${x}=${b}$SLURM_BCAST${x}"
    fi

    if [[ -v SLURM_BURST_BUFFER ]]; then
        echo -e "export ${b}SLURM_BURST_BUFFER${x}=${b}$SLURM_BURST_BUFFER${x}"
    fi

    if [[ -v SLURM_CLUSTERS ]]; then
        echo -e "export ${b}SLURM_CLUSTERS${x}=${b}$SLURM_CLUSTERS${x}"
    fi

    if [[ -v SLURM_COMPRESS ]]; then
        echo -e "export ${b}SLURM_COMPRESS${x}=${b}$SLURM_COMPRESS${x}"
    fi

    if [[ -v SLURM_CONF ]]; then
        echo -e "export ${b}SLURM_CONF${x}=${b}$SLURM_CONF${x}"
    fi

    if [[ -v SLURM_CONSTRAINT ]]; then
        echo -e "export ${b}SLURM_CONSTRAINT${x}=${b}$SLURM_CONSTRAINT${x}"
    fi

    if [[ -v SLURM_CORE_SPEC ]]; then
        echo -e "export ${b}SLURM_CORE_SPEC${x}=${b}$SLURM_CORE_SPEC${x}"
    fi

    if [[ -v SLURM_CPU_BIND ]]; then
        echo -e "export ${b}SLURM_CPU_BIND${x}=${b}$SLURM_CPU_BIND${x}"
    fi

    if [[ -v SLURM_CPU_FREQ_REQ ]]; then
        echo -e "export ${b}SLURM_CPU_FREQ_REQ${x}=${b}$SLURM_CPU_FREQ_REQ${x}"
    fi

    if [[ -v SLURM_CPUS_PER_GPU ]]; then
        echo -e "export ${b}SLURM_CPUS_PER_GPU${x}=${b}$SLURM_CPUS_PER_GPU${x}"
    fi

    if [[ -v SLURM_CPUS_PER_TASK ]]; then
        echo -e "export ${b}SLURM_CPUS_PER_TASK${x}=${b}$SLURM_CPUS_PER_TASK${x}"
    fi

    if [[ -v SLURM_DEBUG ]]; then
        echo -e "export ${b}SLURM_DEBUG${x}=${b}$SLURM_DEBUG${x}"
    fi

    if [[ -v SLURM_DELAY_BOOT ]]; then
        echo -e "export ${b}SLURM_DELAY_BOOT${x}=${b}$SLURM_DELAY_BOOT${x}"
    fi

    if [[ -v SLURM_DEPENDENCY ]]; then
        echo -e "export ${b}SLURM_DEPENDENCY${x}=${b}$SLURM_DEPENDENCY${x}"
    fi

    if [[ -v SLURM_DISABLE_STATUS ]]; then
        echo -e "export ${b}SLURM_DISABLE_STATUS${x}=${b}$SLURM_DISABLE_STATUS${x}"
    fi

    if [[ -v SLURM_DIST_PLANESIZE ]]; then
        echo -e "export ${b}SLURM_DIST_PLANESIZE${x}=${b}$SLURM_DIST_PLANESIZE${x}"
    fi

    if [[ -v SLURM_DISTRIBUTION ]]; then
        echo -e "export ${b}SLURM_DISTRIBUTION${x}=${b}$SLURM_DISTRIBUTION${x}"
    fi

    if [[ -v SLURM_EPILOG ]]; then
        echo -e "export ${b}SLURM_EPILOG${x}=${b}$SLURM_EPILOG${x}"
    fi

    if [[ -v SLURM_EXACT ]]; then
        echo -e "export ${b}SLURM_EXACT${x}=${b}$SLURM_EXACT${x}"
    fi

    if [[ -v SLURM_EXCLUSIVE ]]; then
        echo -e "export ${b}SLURM_EXCLUSIVE${x}=${b}$SLURM_EXCLUSIVE${x}"
    fi

    if [[ -v SLURM_EXIT_ERROR ]]; then
        echo -e "export ${b}SLURM_EXIT_ERROR${x}=${b}$SLURM_EXIT_ERROR${x}"
    fi

    if [[ -v SLURM_EXIT_IMMEDIATE ]]; then
        echo -e "export ${b}SLURM_EXIT_IMMEDIATE${x}=${b}$SLURM_EXIT_IMMEDIATE${x}"
    fi

    if [[ -v SLURM_EXPORT_ENV ]]; then
        echo -e "export ${b}SLURM_EXPORT_ENV${x}=${b}$SLURM_EXPORT_ENV${x}"
    fi

    if [[ -v SLURM_GPU_BIND ]]; then
        echo -e "export ${b}SLURM_GPU_BIND${x}=${b}$SLURM_GPU_BIND${x}"
    fi

    if [[ -v SLURM_GPU_FREQ ]]; then
        echo -e "export ${b}SLURM_GPU_FREQ${x}=${b}$SLURM_GPU_FREQ${x}"
    fi

    if [[ -v SLURM_GPUS ]]; then
        echo -e "export ${b}SLURM_GPUS${x}=${b}$SLURM_GPUS${x}"
    fi

    if [[ -v SLURM_GPUS_PER_NODE ]]; then
        echo -e "export ${b}SLURM_GPUS_PER_NODE${x}=${b}$SLURM_GPUS_PER_NODE${x}"
    fi

    if [[ -v SLURM_GPUS_PER_TASK ]]; then
        echo -e "export ${b}SLURM_GPUS_PER_TASK${x}=${b}$SLURM_GPUS_PER_TASK${x}"
    fi

    if [[ -v SLURM_GRES ]]; then
        echo -e "export ${b}SLURM_GRES${x}=${b}$SLURM_GRES${x}"
    fi

    if [[ -v SLURM_GRES_FLAGS ]]; then
        echo -e "export ${b}SLURM_GRES_FLAGS${x}=${b}$SLURM_GRES_FLAGS${x}"
    fi

    if [[ -v SLURM_HINT ]]; then
        echo -e "export ${b}SLURM_HINT${x}=${b}$SLURM_HINT${x}"
    fi

    if [[ -v SLURM_IMMEDIATE ]]; then
        echo -e "export ${b}SLURM_IMMEDIATE${x}=${b}$SLURM_IMMEDIATE${x}"
    fi

    if [[ -v SLURM_JOB_ID ]]; then
        echo -e "export ${b}SLURM_JOB_ID${x}=${b}$SLURM_JOB_ID${x}"
    fi

    if [[ -v SLURM_JOB_NAME ]]; then
        echo -e "export ${b}SLURM_JOB_NAME${x}=${b}$SLURM_JOB_NAME${x}"
    fi

    if [[ -v SLURM_JOB_NODELIST ]]; then
        echo -e "export ${b}SLURM_JOB_NODELIST${x}=${b}$SLURM_JOB_NODELIST${x}"
    fi

    if [[ -v SLURM_JOB_NUM_NODES ]]; then
        echo -e "export ${b}SLURM_JOB_NUM_NODES${x}=${b}$SLURM_JOB_NUM_NODES${x}"
    fi

    if [[ -v SLURM_KILL_BAD_EXIT ]]; then
        echo -e "export ${b}SLURM_KILL_BAD_EXIT${x}=${b}$SLURM_KILL_BAD_EXIT${x}"
    fi

    if [[ -v SLURM_LABELIO ]]; then
        echo -e "export ${b}SLURM_LABELIO${x}=${b}$SLURM_LABELIO${x}"
    fi

    if [[ -v SLURM_MEM_BIND ]]; then
        echo -e "export ${b}SLURM_MEM_BIND${x}=${b}$SLURM_MEM_BIND${x}"
    fi

    if [[ -v SLURM_MEM_PER_CPU ]]; then
        echo -e "export ${b}SLURM_MEM_PER_CPU${x}=${b}$SLURM_MEM_PER_CPU${x}"
    fi

    if [[ -v SLURM_MEM_PER_GPU ]]; then
        echo -e "export ${b}SLURM_MEM_PER_GPU${x}=${b}$SLURM_MEM_PER_GPU${x}"
    fi

    if [[ -v SLURM_MEM_PER_NODE ]]; then
        echo -e "export ${b}SLURM_MEM_PER_NODE${x}=${b}$SLURM_MEM_PER_NODE${x}"
    fi

    if [[ -v SLURM_MPI_TYPE ]]; then
        echo -e "export ${b}SLURM_MPI_TYPE${x}=${b}$SLURM_MPI_TYPE${x}"
    fi

    if [[ -v SLURM_NETWORK ]]; then
        echo -e "export ${b}SLURM_NETWORK${x}=${b}$SLURM_NETWORK${x}"
    fi

    if [[ -v SLURM_NNODES ]]; then
        echo -e "export ${b}SLURM_NNODES${x}=${b}$SLURM_NNODES${x}"
    fi

    if [[ -v SLURM_NO_KILL ]]; then
        echo -e "export ${b}SLURM_NO_KILL${x}=${b}$SLURM_NO_KILL${x}"
    fi

    if [[ -v SLURM_NPROCS ]]; then
        echo -e "export ${b}SLURM_NPROCS${x}=${b}$SLURM_NPROCS${x}"
    fi

    if [[ -v SLURM_NTASKS ]]; then
        echo -e "export ${b}SLURM_NTASKS${x}=${b}$SLURM_NTASKS${x}"
    fi

    if [[ -v SLURM_NTASKS_PER_CORE ]]; then
        echo -e "export ${b}SLURM_NTASKS_PER_CORE${x}=${b}$SLURM_NTASKS_PER_CORE${x}"
    fi

    if [[ -v SLURM_NTASKS_PER_GPU ]]; then
        echo -e "export ${b}SLURM_NTASKS_PER_GPU${x}=${b}$SLURM_NTASKS_PER_GPU${x}"
    fi

    if [[ -v SLURM_NTASKS_PER_NODE ]]; then
        echo -e "export ${b}SLURM_NTASKS_PER_NODE${x}=${b}$SLURM_NTASKS_PER_NODE${x}"
    fi

    if [[ -v SLURM_NTASKS_PER_SOCKET ]]; then
        echo -e "export ${b}SLURM_NTASKS_PER_SOCKET${x}=${b}$SLURM_NTASKS_PER_SOCKET${x}"
    fi

    if [[ -v SLURM_OPEN_MODE ]]; then
        echo -e "export ${b}SLURM_OPEN_MODE${x}=${b}$SLURM_OPEN_MODE${x}"
    fi

    if [[ -v SLURM_OVERCOMMIT ]]; then
        echo -e "export ${b}SLURM_OVERCOMMIT${x}=${b}$SLURM_OVERCOMMIT${x}"
    fi

    if [[ -v SLURM_OVERLAP ]]; then
        echo -e "export ${b}SLURM_OVERLAP${x}=${b}$SLURM_OVERLAP${x}"
    fi

    if [[ -v SLURM_PARTITION ]]; then
        echo -e "export ${b}SLURM_PARTITION${x}=${b}$SLURM_PARTITION${x}"
    fi

    if [[ -v SLURM_PMI_KVS_NO_DUP_KEYS ]]; then
        echo -e "export ${b}SLURM_PMI_KVS_NO_DUP_KEYS${x}=${b}$SLURM_PMI_KVS_NO_DUP_KEYS${x}"
    fi

    if [[ -v SLURM_POWER ]]; then
        echo -e "export ${b}SLURM_POWER${x}=${b}$SLURM_POWER${x}"
    fi

    if [[ -v SLURM_PROFILE ]]; then
        echo -e "export ${b}SLURM_PROFILE${x}=${b}$SLURM_PROFILE${x}"
    fi

    if [[ -v SLURM_PROLOG ]]; then
        echo -e "export ${b}SLURM_PROLOG${x}=${b}$SLURM_PROLOG${x}"
    fi

    if [[ -v SLURM_QOS ]]; then
        echo -e "export ${b}SLURM_QOS${x}=${b}$SLURM_QOS${x}"
    fi

    if [[ -v SLURM_REMOTE_CWD ]]; then
        echo -e "export ${b}SLURM_REMOTE_CWD${x}=${b}$SLURM_REMOTE_CWD${x}"
    fi

    if [[ -v SLURM_REQ_SWITCH ]]; then
        echo -e "export ${b}SLURM_REQ_SWITCH${x}=${b}$SLURM_REQ_SWITCH${x}"
    fi

    if [[ -v SLURM_RESERVATION ]]; then
        echo -e "export ${b}SLURM_RESERVATION${x}=${b}$SLURM_RESERVATION${x}"
    fi

    if [[ -v SLURM_RESV_PORTS ]]; then
        echo -e "export ${b}SLURM_RESV_PORTS${x}=${b}$SLURM_RESV_PORTS${x}"
    fi

    if [[ -v SLURM_SIGNAL ]]; then
        echo -e "export ${b}SLURM_SIGNAL${x}=${b}$SLURM_SIGNAL${x}"
    fi

    if [[ -v SLURM_SPREAD_JOB ]]; then
        echo -e "export ${b}SLURM_SPREAD_JOB${x}=${b}$SLURM_SPREAD_JOB${x}"
    fi

    if [[ -v SLURM_SRUN_REDUCE_TASK_EXIT_MSG ]]; then
        echo -e "export ${b}SLURM_SRUN_REDUCE_TASK_EXIT_MSG${x}=${b}$SLURM_SRUN_REDUCE_TASK_EXIT_MSG${x}"
    fi

    if [[ -v SLURM_STDERRMODE ]]; then
        echo -e "export ${b}SLURM_STDERRMODE${x}=${b}$SLURM_STDERRMODE${x}"
    fi

    if [[ -v SLURM_STDINMODE ]]; then
        echo -e "export ${b}SLURM_STDINMODE${x}=${b}$SLURM_STDINMODE${x}"
    fi

    if [[ -v SLURM_STDOUTMODE ]]; then
        echo -e "export ${b}SLURM_STDOUTMODE${x}=${b}$SLURM_STDOUTMODE${x}"
    fi

    if [[ -v SLURM_STEP_GRES ]]; then
        echo -e "export ${b}SLURM_STEP_GRES${x}=${b}$SLURM_STEP_GRES${x}"
    fi

    if [[ -v SLURM_STEP_KILLED_MSG_NODE_ID ]]; then
        echo -e "export ${b}SLURM_STEP_KILLED_MSG_NODE_ID${x}=${b}$SLURM_STEP_KILLED_MSG_NODE_ID${x}"
    fi

    if [[ -v SLURM_TASK_EPILOG ]]; then
        echo -e "export ${b}SLURM_TASK_EPILOG${x}=${b}$SLURM_TASK_EPILOG${x}"
    fi

    if [[ -v SLURM_TASK_PROLOG ]]; then
        echo -e "export ${b}SLURM_TASK_PROLOG${x}=${b}$SLURM_TASK_PROLOG${x}"
    fi

    if [[ -v SLURM_TEST_EXEC ]]; then
        echo -e "export ${b}SLURM_TEST_EXEC${x}=${b}$SLURM_TEST_EXEC${x}"
    fi

    if [[ -v SLURM_THREAD_SPEC ]]; then
        echo -e "export ${b}SLURM_THREAD_SPEC${x}=${b}$SLURM_THREAD_SPEC${x}"
    fi

    if [[ -v SLURM_THREADS ]]; then
        echo -e "export ${b}SLURM_THREADS${x}=${b}$SLURM_THREADS${x}"
    fi

    if [[ -v SLURM_THREADS_PER_CORE ]]; then
        echo -e "export ${b}SLURM_THREADS_PER_CORE${x}=${b}$SLURM_THREADS_PER_CORE${x}"
    fi

    if [[ -v SLURM_TIMELIMIT ]]; then
        echo -e "export ${b}SLURM_TIMELIMIT${x}=${b}$SLURM_TIMELIMIT${x}"
    fi

    if [[ -v SLURM_UNBUFFEREDIO ]]; then
        echo -e "export ${b}SLURM_UNBUFFEREDIO${x}=${b}$SLURM_UNBUFFEREDIO${x}"
    fi

    if [[ -v SLURM_USE_MIN_NODES ]]; then
        echo -e "export ${b}SLURM_USE_MIN_NODES${x}=${b}$SLURM_USE_MIN_NODES${x}"
    fi

    if [[ -v SLURM_WAIT ]]; then
        echo -e "export ${b}SLURM_WAIT${x}=${b}$SLURM_WAIT${x}"
    fi

    if [[ -v SLURM_WAIT4SWITCH ]]; then
        echo -e "export ${b}SLURM_WAIT4SWITCH${x}=${b}$SLURM_WAIT4SWITCH${x}"
    fi

    if [[ -v SLURM_WCKEY ]]; then
        echo -e "export ${b}SLURM_WCKEY${x}=${b}$SLURM_WCKEY${x}"
    fi

    if [[ -v SLURM_WHOLE ]]; then
        echo -e "export ${b}SLURM_WHOLE${x}=${b}$SLURM_WHOLE${x}"
    fi

    if [[ -v SLURM_WORKING_DIR ]]; then
        echo -e "export ${b}SLURM_WORKING_DIR${x}=${b}$SLURM_WORKING_DIR${x}"
    fi

    if [[ -v SLURMD_DEBUG ]]; then
        echo -e "export ${b}SLURMD_DEBUG${x}=${b}$SLURMD_DEBUG${x}"
    fi

    if [[ -v SRUN_EXPORT_ENV ]]; then
        echo -e "export ${b}SRUN_EXPORT_ENV${x}=${b}$SRUN_EXPORT_ENV${x}"
    fi
fi
