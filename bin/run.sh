#!/usr/bin/env bash

set -e
set -o pipefail

CURRENT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
source "$CURRENT_DIR/../etc/defaults.sh"
source "$CURRENT_DIR/common.sh"

file="$1"
stage="$2"

[[ ! -x "$file" ]] && echo "File $file is not executable" && exit $BUILD_FAILURE_EXIT_CODE

if [[ "${stage}" == "build_script" || "${stage}" == "step_script" || "${stage}" == "after_script" ]]; then
    source "$CURRENT_DIR/setup_slurm.sh"

    # send job meta data to middleware
    if [[ -z "$CLUSTER_NAME" && -f /etc/xthostname ]] ; then
        CLUSTER_NAME=$(cat /etc/xthostname)
    fi
    JOB_META_DATA='{"mode":"login-baremetal","machine":"'$CLUSTER_NAME'"}'
    curl -s --retry 5 --retry-connrefused -H "Content-Type: application/json" --data-raw "${JOB_META_DATA}" "${CUSTOM_ENV_CSCS_CI_MW_URL}/redis/job?token=${CUSTOM_ENV_CI_JOB_TOKEN}&job_id=${CUSTOM_ENV_CI_JOB_ID}"

#    bash -c "$file"
    "${CLEANENV[@]}" bash -lc "$file"

    result=$?

    # Exit using the variable, to make the build as failure in GitLab CI.
    if [ "$result" -ne 0 ]; then
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi
elif [[ "${stage}" == "get_sources" ]]; then
    # retry getting sources up to 3 times, deleting the whole folder in between retries
    retry "$1" 3 random 10 30 "rm -Rf '$CUSTOM_ENV_CI_BUILDS_DIR'"
else
  ${1}
fi

exit 0
